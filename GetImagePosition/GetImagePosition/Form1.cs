﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GetImagePosition
{
    public partial class Form1 : Form
    {
        //string filePath;
        private Point m_ImgDrawPoint;
        private Point m_ImgTmpPoint;
        private Point m_MouseDownPoint;
        private Image m_Image;
        private bool m_MouseInImage;
        string savefile;

        Form2 fm2 = new Form2();

        public Form1()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {

                if (this.m_Image != null)
                {
                    m_MouseDownPoint = e.Location;
                    this.m_ImgTmpPoint = this.m_ImgDrawPoint;
                    Rectangle rect = new Rectangle(this.m_ImgDrawPoint.X, this.m_ImgDrawPoint.Y, this.m_Image.Width, this.m_Image.Height);
                    m_MouseInImage = rect.Contains(e.Location);
                    if (m_MouseInImage)
                    {
                        Point msPoint = e.Location;
                        msPoint.Offset(-this.m_ImgDrawPoint.X, -this.m_ImgDrawPoint.Y);
                        //toolStripStatusLabel1.Text = string.Format("鼠标在图片上的位置：{0}", msPoint.ToString());

                        fm2.xpoint = msPoint.X;
                        fm2.ypoint = msPoint.Y;
                        fm2.StartPosition = FormStartPosition.CenterParent;


                        fm2.ShowDialog();


                    }
                }
            }
        }

        private void toolStripLabel2_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = "图像文件(*.jpg;*.gif;*.bmp;*.png)|*.jpg;*.gif;*.bmp;*.png";
            if (of.ShowDialog() == DialogResult.OK)
            {
                m_ImgDrawPoint = Point.Empty;
                //filePath = of.FileName;

                if (this.m_Image != null)
                {
                    this.m_Image.Dispose();
                }

                m_Image = Image.FromFile(of.FileName);

                pictureBox1.Image = m_Image;
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = "c:\\";
            sfd.Filter = "txt文件(*.txt)|*.txt";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                savefile = sfd.FileName;

                try
                {
                    if (!File.Exists(savefile))
                    {
                        FileStream fs = File.Create(savefile);
                        fs.Close();
                    }

                    FileStream f = new FileStream(savefile, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                    StreamWriter sw = new StreamWriter(f);

                    foreach (string line in fm2.listr)
                    {
                        sw.WriteLine(line);
                    }

                    sw.Flush();
                    sw.Close();
                    f.Close();
                }
                catch
                { }

            }
        }
    }
}
