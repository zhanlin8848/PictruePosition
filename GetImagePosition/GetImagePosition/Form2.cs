﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GetImagePosition
{
    public partial class Form2 : Form
    {
        public int xpoint = 0;
        public int ypoint = 0;
        public List<string> listr = new List<string>();
        //string savefile;
        public Form2()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string line = "x:" + xpoint + ",y:" + ypoint + "," + textBox1.Text;
            listr.Add(line);

            this.DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void button1_Paint(object sender, PaintEventArgs e)
        {
            label1.Text = "鼠标在图片上的位置是 x:" + xpoint + ", y:" + ypoint;
        }
    }
}
